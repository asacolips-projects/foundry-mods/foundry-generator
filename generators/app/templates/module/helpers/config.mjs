export const <%= constant_name %> = {};

// Define constants here, such as:
<%= constant_name %>.foobar = {
  'bas': '<%= constant_name %>.bas',
  'bar': '<%= constant_name %>.bar'
};<% if (includeAbil) { %>

/**
 * The set of Ability Scores used within the sytem.
 * @type {Object}
 */
 <%= constant_name %>.abilities = {
  "str": "<%= constant_name %>.AbilityStr",
  "dex": "<%= constant_name %>.AbilityDex",
  "con": "<%= constant_name %>.AbilityCon",
  "int": "<%= constant_name %>.AbilityInt",
  "wis": "<%= constant_name %>.AbilityWis",
  "cha": "<%= constant_name %>.AbilityCha"
};

<%= constant_name %>.abilityAbbreviations = {
  "str": "<%= constant_name %>.AbilityStrAbbr",
  "dex": "<%= constant_name %>.AbilityDexAbbr",
  "con": "<%= constant_name %>.AbilityConAbbr",
  "int": "<%= constant_name %>.AbilityIntAbbr",
  "wis": "<%= constant_name %>.AbilityWisAbbr",
  "cha": "<%= constant_name %>.AbilityChaAbbr"
};<% } %>