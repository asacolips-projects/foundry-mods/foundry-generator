'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const glob = require('glob');

module.exports = class extends Generator {

  prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(`Welcome to the awe-inspiring ${chalk.red('foundry')} system generator!`)
    );

    const prompts = [
      {
        type: 'input',
        name: 'machine_name',
        message: 'What\'s the machine-safe name of your system?',
        default: 'mysystem'
      },
      {
        type: 'input',
        name: 'name',
        message: 'What\'s the full name of your system?',
        default: 'My System'
      },
      {
        type: 'input',
        name: 'class_name',
        message: 'What name should be used for JS classes? (alphabetic characters only)',
        default: 'MySystem'
      },
      {
        type: 'input',
        name: 'author',
        message: 'Author:',
        default: 'John Doe'
      },
      {
        type: 'checkbox',
        name: 'features',
        message: 'Choose optional features:',
        choices: [
          {value: 'activeEffects', name: 'Active Effects', checked: true},
          {value: 'itemMacros', name: 'Item roll macrobar support', checked: true},
          {value: 'npc', name: 'NPC actor type'},
          {value: 'abil', name: 'Example: d20 Ability Score modifiers'},
          {value: 'spells', name: 'Example: Spells on characters'}
        ],
      }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
      // TODO: Turn these into prompts.
      this.props.git_url = '';
      this.props.manifest_url = '';
      this.props.zip_url = '';

      const hasFeature = feat => props.features && props.features.includes(feat);

      this.props.includeActiveEffects = hasFeature('activeEffects');
      this.props.includeItemMacros = hasFeature('itemMacros');
      this.props.includeNpc = hasFeature('npc');
      this.props.includeAbil = hasFeature('abil');
      this.props.includeSpells = hasFeature('spells');

      this.props.constant_name = this.props.machine_name.toUpperCase();
    });
  }

  writing() {
    const files = glob.sync('**', { dot: true, nodir: true, cwd: this.templatePath() })
    for (let i in files) {
      let destination = this.props.destinationFolderPath ? this.props.destinationFolderPath + '/' : '';
      // Skip spells if needed.
      if (!this.props.includeSpells && files[i].includes('spell')) continue;
      // Skip npc if needed.
      if (!this.props.includeNpc && files[i].includes('npc')) continue;
      // Skip active effects if needed.
      if (!this.props.includeActiveEffects && files[i].includes('effect')) continue;
      // Copy files.
      this.fs.copyTpl(
        this.templatePath(files[i]),
        this.destinationPath(destination + files[i].replace(/__machine_name__/g, this.props.machine_name).replace(/__/g, '')),
        this.props
      )
    }
  }

  paths() {
    this.destinationRoot(this.contextRoot + '/' + this.props.machine_name)
  }

  // install() {
  //   this.npmInstall();
  // }
};
