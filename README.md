# generator-foundry [![NPM version][npm-image]][npm-url]

Generate systems for FoundryVTT

## Installation

First, install [Yeoman](http://yeoman.io) and generator-foundry using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-foundry
```

Then generate your new project:

```bash
yo foundry
```

The generator will prompt you for several questions, such as your system's machine name and what name should be used as the prefix on Javascript classes. The built system also includes a simple package.json and gulpfile.js, so that you can compile the scss by using `npm install` and `npm run gulp` in the generated system's directory.

![image](http://mattsmith.in/images/foundry-generator.png)

## License

MIT © [asacolips](mattsmith.in)

[npm-image]: https://badge.fury.io/js/generator-foundry.svg
[npm-url]: https://npmjs.org/package/generator-foundry
